# ✅ Test Report (Success)

## Overview

| Status | Total | Passed | Failed | Duration (seconds) |
| --- | --- | --- | --- | --- |
| ✅ Success | 2 | 2 | 0 | 0.002 |

🎓 **Project:**  mlops-1-bank-customer-churn 
⚡ **Branch:**  step-1-versioning 

## Tests

| Name | Status | Description  |
| --- | --- | --- |
| Task 2 - Add a Train dataset to the DVC version control | ✅ Success | Great job!   |
| Task 3 - Log models to DVC and MLflow |  ✅ Success | Great job!   |
| Task 4 - Зарегистрировать random-forest модель в MR (MLflow) | ✅ Success | Great job!   | 
| Task 5 - Зарегистрировать xgboost  модель  в MR (DVC/GTO) |  ✅ Success | Great job!   |

### Congratulations! All tests passed! 🎉

![Untitled](https://raw.githubusercontent.com/mlrepa/mlrepa-library/main/docs/static/testing/banner-success.png)
